import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

/* translate */ import { JsonService } from './services/json.service';

@NgModule({
    imports: [
        // modules
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        JsonpModule,
        HttpModule,
        HttpClientModule        
    ],
    declarations: [],
    entryComponents: [],
    providers: [JsonService],
    exports: [
        // modules
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule      
    ]
})

export class SharedModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                JsonService
            ]
        };
    }
}
