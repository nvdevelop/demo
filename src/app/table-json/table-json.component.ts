import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JsonService } from '../shared/services/json.service';

import { PersonModel } from '../shared/models/person.model';

@Component({
    selector: 'cmp-table-json',
    templateUrl: 'table-json.component.html',
    styleUrls: ['./table-json.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class TableJsonComponent implements OnInit {

    tableData: PersonModel[] = [];

    constructor(
        private jsonService: JsonService
    ) { }

    ngOnInit(){
        this.jsonService.getJSON().subscribe(
            (response: PersonModel[]) => {
                if(response) {
                    this.tableData = response;
                    console.log(response);
                }
            },
            (error: any) => {
                console.log(error);
                alert("Error message");
            }
        );
    }
}
