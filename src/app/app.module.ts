import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/* component */  import { AppComponent } from './app.component';
/* component */  import { TableJsonComponent } from './table-json/table-json.component';
/* module */ import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    TableJsonComponent
  ],
  imports: [
    BrowserModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
